# EDT ADE Esisar
Ce script récupère les EDTs de l'ESISAR sur edt.grenoble-inp.fr avec une authentification pour avoir le nom des professeurs, puis génère des calendriers ICS en réecrivant la ligne DESCRIPTION de chaque cours en essayant de la formatter joliment. (Vu que y a environ 600 exceptions, c'est pas super simple...)
Une page est aussi disponible pour trouver des salles libres à un créneau demandé le jour même. L'authentification n'est pas forcément obligatoire, mais j'ai copié collé le code.
Ce script est servi sur [https://edt.kinsteen.fr].

## Install
Il faut le module `ConfigParser` et `urllib3` pour que le script fonctionne.
Copiez le fichier de config (par exemple : `cp config.ini.example config.ini`)  
Rentrez vos identifiants Agalan, puis lancer le script. Tous les calendriers seront générés dans le dossier `output` par défaut.

### Pour les changements d'années
Le script ne gère pas la génération des ID des emplois du temps, dans le fichier `IDS.txt`. Vous pouvez essayer de l'automatiser si vous le voulez.
Il y a des tutos qui trainent chez le CIE pour le faire :)

## Contributing
Quelques cas d'exceptions correctement parsés ont été fait pour les 1A, les 2A et les 3A.
Il en reste encore pour les 4A et 5A. Libre à vous de les implémenter !