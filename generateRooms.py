import configparser
import urllib3
from datetime import datetime
import re

def generateEDT(id):
    month = datetime.now().month
    year = datetime.now().year if month > 8 else datetime.now().year - 1 # Proper check for current year

    http = urllib3.PoolManager()
    headers = urllib3.util.make_headers(basic_auth=config.get('AGALAN', 'AGALAN_LOGIN') + ":" + config.get('AGALAN', 'AGALAN_PASSWORD'))
    r = http.request(
        'GET',
        f'https://edt.grenoble-inp.fr/directCal/{year}-{year + 1}/etudiant/esisar?resources={id}&startDay=31&startMonth=08&startYear={year - 2}&endDay=10&endMonth=01&endYear={year + 3}',
        headers=headers)

    result = r.data.decode('utf-8').splitlines()
    return result

config = configparser.ConfigParser()
config.read('config.ini')

# opening ID file
ids = open('Rooms-IDS.txt', 'r')

for id in ids:
    data = generateEDT(int(id.split(',')[1]))
    f = open(config.get("Config", "RoomsOutputDir") + id.split(',')[0] + ".ics", 'w')
    f.write("\n".join(data))
    f.close()
    print("Generated " + id.split(',')[0] + ".ics !")

print("Finished.")
